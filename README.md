# git bisect examples

## Checking manually

- initialize

```shell
$ git bisect start
$ git bisect good v1.0.0
$ git bisect bad v1.0.1
Bisecting: 0 revisions left to test after this (roughly 0 steps)
[8418a5efc84a5d1ac0811a805a6d1777a7633919] I spilled marmelade
```

- open browser to check
- continue with `git bisect good` or `git bisect bad` until

```
2a0aa529b858ed7e9b0458b9f583f71f4c67b463 is the first bad commit
commit 2a0aa529b858ed7e9b0458b9f583f71f4c67b463
Author: winh <winnie@gitlab.com>
Date:   Tue Aug 8 14:25:12 2017 +0200

    Green looks so much nicer

:100644 100644 3fe4f38d2030beddae19dc7415bb77ab4befc09c 646f420d57ce918353f6ab628d2aa9a1e9dd7d3b M	index.html
```

## Checking automatically

- initialize

```shell
$ git bisect start
$ git bisect good v1.0.0
$ git bisect bad v1.0.1
Bisecting: 0 revisions left to test after this (roughly 0 steps)
[8418a5efc84a5d1ac0811a805a6d1777a7633919] I spilled marmelade
```

- use a script to check

```shell
$ git bisect run grep manager index.html
running grep manager index.html
8418a5efc84a5d1ac0811a805a6d1777a7633919 is the first bad commit
commit 8418a5efc84a5d1ac0811a805a6d1777a7633919
Author: winh <winnie@gitlab.com>
Date:   Tue Aug 8 14:24:19 2017 +0200

    I spilled marmelade

:100644 100644 007a412ac4e48cce3f03104c12fa5dd48d046ada 3fe4f38d2030beddae19dc7415bb77ab4befc09c M	index.html
bisect run success
```
